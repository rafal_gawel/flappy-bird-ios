//
//  GameScene.swift
//  FlappyBird
//
//  Created by Rafał Gaweł on 10/03/2018.
//  Copyright © 2018 Rafał Gaweł. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var scoreLabel : SKLabelNode!
    private var birdNode: SKSpriteNode!
    
    private var started = false
    private var gameOver = false
    
    private var spawnTimer: Timer!
    private var lastHoleCenterY: CGFloat = 0
    private var passedPipes = 0
    
    private let birdCategory: UInt32 = 0x1 << 0
    private let pipeCategory: UInt32 = 0x1 << 1
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        setupBackgroundNode(view: view)
        setupScoreLabel()
        setupBirdNode()
        
        lastHoleCenterY = frame.midY
    }
    
    private func setupBackgroundNode(view: SKView) {
        let background = SKSpriteNode(imageNamed: "background")
        background.anchorPoint = CGPoint(x: 0.0, y: 1.0)
        background.position = CGPoint(x: 0.0, y: size.height)
        background.aspectFillToSize(fillSize: view.frame.size)
        
        addChild(background)
    }
    
    private func setupScoreLabel() {
        // http://iosfonts.com/
        scoreLabel = SKLabelNode(fontNamed: "AvenirNext-Bold")
        scoreLabel.text = "0"
        scoreLabel.fontSize = 50.0
        scoreLabel.fontColor = SKColor.white
        scoreLabel.position = CGPoint(x: frame.midX, y: frame.height - 50)
        scoreLabel.zPosition = 1.0
        
        addChild(scoreLabel)
    }
    
    private func setupBirdNode() {
        birdNode = SKSpriteNode(imageNamed: "bird")
        birdNode.position = CGPoint(x: frame.midX, y: frame.midY)
        
        birdNode.physicsBody = SKPhysicsBody(circleOfRadius: birdNode.size.height / 2.0)
        birdNode.physicsBody?.categoryBitMask = birdCategory
        birdNode.physicsBody?.contactTestBitMask = pipeCategory
        birdNode.physicsBody?.isDynamic = false
        
        addChild(birdNode!)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard !gameOver else {
            return
        }
        
        if !started {
            birdNode.physicsBody?.isDynamic = true
            started = true
            startSpawningPipes()
        }
        
        //birdNode.physicsBody?.applyImpulse(CGVector(dx: 0.0, dy: 50.0))
        birdNode.physicsBody?.velocity = CGVector(dx: 0.0, dy: 500.0)
        
        run(SKAction.playSoundFileNamed("sfx_wing", waitForCompletion: false))
    }
    
    private func startSpawningPipes() {
        spawnTimer = Timer.scheduledTimer(timeInterval: 1.5,
                                          target: self,
                                          selector: #selector(addPipe),
                                          userInfo: nil,
                                          repeats: true)
    }
    
    @objc private func addPipe() {
        var holeCenterY = lastHoleCenterY + CGFloat(GKRandomDistribution(lowestValue: -50, highestValue: 50).nextInt())
        
        if holeCenterY < frame.midY - 200 {
            holeCenterY = frame.midY - 200
        }
        
        if holeCenterY > frame.midY + 200 {
            holeCenterY = frame.midY + 200
        }
        
        lastHoleCenterY = holeCenterY
        
        let holeSize: CGFloat = 200.0
        

        let upperPipe = createPipe(endY: holeCenterY + holeSize / 2.0)
        addChild(upperPipe)
        
        let lowerPipe = createPipe(endY: holeCenterY - holeSize / 2.0)
        lowerPipe.yScale = -1.0
        
        addChild(lowerPipe)
    }
    
    private func createPipe(endY: CGFloat) -> SKSpriteNode {
        let spawnDistance: CGFloat = 400
        
        let pipeNode = SKSpriteNode(imageNamed: "pipe")
        pipeNode.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        pipeNode.position = CGPoint(x: frame.midX + spawnDistance, y: endY)
        
        let size = pipeNode.size
        
        pipeNode.physicsBody = SKPhysicsBody(rectangleOf: size, center: CGPoint(x: 0.0, y: size.height/2))
        pipeNode.physicsBody?.isDynamic = false
        pipeNode.physicsBody?.categoryBitMask = pipeCategory
        pipeNode.physicsBody?.contactTestBitMask = birdCategory
        
        pipeNode.run(SKAction.sequence([
            SKAction.move(to: CGPoint(x: frame.midX, y: endY), duration: 3.0),
            SKAction.run { [weak self] in self?.didPassPipe() },
            SKAction.move(to: CGPoint(x: frame.midX - spawnDistance, y: endY), duration: 3.0),
            SKAction.removeFromParent()
            ]))
        
        return pipeNode
    }
    
    private func didPassPipe() {
        guard !gameOver else {
            return
        }
        
        passedPipes += 1
        let score = passedPipes / 2
        scoreLabel.text = "\(score)"
        
        if passedPipes % 2 == 1 {
            run(SKAction.playSoundFileNamed("sfx_point", waitForCompletion: false))
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        guard !gameOver else {
            return
        }
        
        guard contact.bodyA === birdNode.physicsBody || contact.bodyB === birdNode.physicsBody else {
            return
        }
        
        birdNode.physicsBody?.angularVelocity = -10.0
        
        let emitter = SKEmitterNode(fileNamed: "Smoke")!
        emitter.zPosition = -1.0
        emitter.targetNode = self
        birdNode.addChild(emitter)
        
        run(SKAction.sequence([
            SKAction.playSoundFileNamed("sfx_hit", waitForCompletion: true),
            SKAction.wait(forDuration: 0.2),
            SKAction.playSoundFileNamed("sfx_die", waitForCompletion: true)
        ]))
        
        gameOver = true
    }

}
