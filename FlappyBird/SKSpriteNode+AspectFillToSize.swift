//
//  SKSpriteNode+AspectFillToSize.swift
//  FlappyBird
//
//  Created by Rafał Gaweł on 10/03/2018.
//  Copyright © 2018 Rafał Gaweł. All rights reserved.
//

import SpriteKit

extension SKSpriteNode {
    
    func aspectFillToSize(fillSize: CGSize) {
        guard let texture = texture else {
            fatalError()
        }
        
        let verticalRatio = fillSize.height / texture.size().height
        let horizontalRatio = fillSize.width /  texture.size().width
        
        let scaleRatio = horizontalRatio > verticalRatio ? horizontalRatio : verticalRatio
        
        self.setScale(scaleRatio)
    }
    
}
